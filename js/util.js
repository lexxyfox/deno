export const DEFAULT_BUFFER_SIZE = 2 ** 15

const utf8 = new TextEncoder()

export const arrayify = x => (Array.isArray(x) ? x : [x])

export const makeUnnamedTempFile = async opts => {
  const path = await Deno.makeTempFile()
  const file = await Deno.open(path, opts)
  await Deno.remove(path)
  return file
}

export const CPIO_MAGIC = 460545

/**
 * Write a number out in cpio format
 * @param {Function} write
 * @param {number|BigInt} val
 * @param {number} [len=8]
 * @returns {number}
 */
export const cpio_write_num = (write, val, len = 8) =>
  write(
    utf8.encode(val.toString(16).slice(-len).padStart(len, '0').toUpperCase()),
  )

const nonnumeric = /\D/
const id = x => x

// not semantic versioning - numeric only; faster (maybe)
export const numvercmp = (a, b) => {
  if (a === b) return 0

  const a2 = a.split(nonnumeric).filter(id).map(Number)
  const b2 = b.split(nonnumeric).filter(id).map(Number)

  const l = Math.min(a2.length, b2.length)
  for (let i = 0; i < l; ++i) {
    const r = a2[i] - b2[i]
    if (r !== 0) return r
  }

  return a2.length - b2.length || a.length - b.length || a.localeCompare(b)
}

const fcre = /^([^\(]*)\((?:([^.\)]*)(?:_([\d.]*))?)?\)(?:\((.*)\))?$/g

export const only_latest_symver = deps => {
  const final_deps = [],
    dep_tree = {}

  for (const dep of deps) {
    const match = [...dep.matchAll(fcre)][0]

    if (match === null) {
      final_deps.push(dep)
      continue
    }
    if (!dep_tree.hasOwnProperty(match[1])) dep_tree[match[1]] = dep
    if (!match[2]) continue
    if (typeof dep_tree[match[1]] === 'string') dep_tree[match[1]] = {}
    if (!dep_tree[match[1]].hasOwnProperty(match[2]))
      dep_tree[match[1]][match[2]] = dep
    if (!match[3]) continue
    if (typeof dep_tree[match[1]][match[2]] === 'string')
      dep_tree[match[1]][match[2]] = {}
    dep_tree[match[1]][match[2]][match[3]] = dep
  }

  for (const [a, b] of Object.entries(dep_tree))
    if (typeof b === 'string') final_deps.push(b)
    else
      for (const [c, d] of Object.entries(b))
        final_deps.push(
          typeof d === 'string'
            ? d
            : d[Object.keys(d).sort(numvercmp).slice(-1)],
        )

  return final_deps.sort()
}

/**
 * 24 bit major + 8 bit minor -> 32 bit dev
 * @param {number} major
 * @param {number} minor
 * @returns {number} dev
 */
export const old_unparse_dev = (major, minor) => (minor & 0xff) | (major << 8)

/**
 * 32 bit dev -> 24 bit major + 8 bit minor
 * @param {number} dev
 */
export const old_parse_dev = dev => ({ major: dev >> 8, minor: dev & 0xff })

/**
 * 12 bit major + 20 bit minor -> 32 bit dev
 * @param {number} major
 * @param {number} minor
 * @returns {number} dev
 */
export const new_unparse_dev = (major, minor) =>
  (minor & 0xff) | ((major & 0xfff) << 8) | ((minor & 0xfff00) << 12)

/**
 * 32 bit dev -> 12 bit major + 20 bit minor
 * @param {number} dev
 */
export const new_parse_dev = dev => ({
  major: (dev >> 8) & 0xfff,
  minor: (dev & 0xff) | ((minor >> 12) & 0xfff00),
})
