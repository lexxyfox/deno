import { Buffer } from 'https://deno.land/std/io/buffer.ts'
import { arrayify } from './util.js'

export const TYPE_AUTOSTRING = -2

// https://github.com/rpm-software-management/rpm/blob/-/lib/rpmtag.h#L440
export const MIN_TYPE = 0
export const TYPE_NULL = 0
export const TYPE_CHAR = 1
export const TYPE_INT8 = 2
export const TYPE_INT16 = 3
export const TYPE_INT32 = 4
export const TYPE_INT64 = 5
export const TYPE_STRING = 6
export const TYPE_BINARY = 7
export const TYPE_STRING_ARRAY = 8
export const TYPE_I18NSTRING = 9
export const MAX_TYPE = 9

// https://github.com/rpm-software-management/rpm/blob/-/lib/rpmtag.h#L16
export const HEADER_IMMUTABLE = 63
export const HEADER_I18NTABLE = 100

// https://github.com/rpm-software-management/rpm/blob/-/lib/rpmtag.h#L412
export const SIG_SIZE = 1000
export const SIG_LEMD5_1 = 1001
export const SIG_PGP = 1002
export const SIG_LEMD5_2 = 1003
export const SIG_MD5 = 1004
export const SIG_GPG = 1005
export const SIG_PGP5 = 1006
export const SIG_PAYLOADSIZE = 1007
export const SIG_RESERVEDSPACE = 1008

// https://github.com/rpm-software-management/rpm/blob/-/lib/rpmtag.h#L73
export const TAG_NAME = 1000
export const TAG_VERSION = 1001
export const TAG_RELEASE = 1002
export const TAG_EPOCH = 1003
export const TAG_SUMMARY = 1004
export const TAG_DESCRIPTION = 1005
export const TAG_BUILDTIME = 1006
export const TAG_BUILDHOST = 1007
export const TAG_INSTALLTIME = 1008
export const TAG_SIZE = 1009
export const TAG_DISTRIBUTION = 1010
export const TAG_VENDOR = 1011
export const TAG_GIF = 1012
export const TAG_XPM = 1013
export const TAG_LICENSE = 1014
export const TAG_PACKAGER = 1015
export const TAG_GROUP = 1016
export const TAG_CHANGELOG = 1017
export const TAG_SOURCE = 1018
export const TAG_PATCH = 1019
export const TAG_URL = 1020
export const TAG_OS = 1021
export const TAG_ARCH = 1022
export const TAG_PREIN = 1023
export const TAG_POSTIN = 1024
export const TAG_PREUN = 1025
export const TAG_POSTUN = 1026
export const TAG_OLDFILENAMES = 1027
export const TAG_FILESIZES = 1028
export const TAG_FILESTATES = 1029
export const TAG_FILEMODES = 1030
export const TAG_FILEUIDS = 1031
export const TAG_FILEGIDS = 1032
export const TAG_FILERDEVS = 1033
export const TAG_FILEMTIMES = 1034
export const TAG_FILEDIGESTS = 1035
export const TAG_FILELINKTOS = 1036
export const TAG_FILEFLAGS = 1037
export const TAG_ROOT = 1038
export const TAG_FILEUSERNAME = 1039
export const TAG_FILEGROUPNAME = 1040
export const TAG_EXCLUDE = 1041
export const TAG_EXCLUSIVE = 1042
export const TAG_ICON = 1043
export const TAG_SOURCERPM = 1044
export const TAG_FILEVERIFYFLAGS = 1045
export const TAG_ARCHIVESIZE = 1046
export const TAG_PROVIDENAME = 1047
export const TAG_REQUIREFLAGS = 1048
export const TAG_REQUIRENAME = 1049
export const TAG_REQUIREVERSION = 1050
export const TAG_NOSOURCE = 1051
export const TAG_NOPATCH = 1052
export const TAG_CONFLICTFLAGS = 1053
export const TAG_CONFLICTNAME = 1054
export const TAG_CONFLICTVERSION = 1055
export const TAG_DEFAULTPREFIX = 1056
export const TAG_BUILDROOT = 1057
export const TAG_INSTALLPREFIX = 1058
export const TAG_EXCLUDEARCH = 1059
export const TAG_EXCLUDEOS = 1060
export const TAG_EXCLUSIVEARCH = 1061
export const TAG_EXCLUSIVEOS = 1062
export const TAG_AUTOREQPROV = 1063
export const TAG_RPMVERSION = 1064
export const TAG_TRIGGERSCRIPTS = 1065
export const TAG_TRIGGERNAME = 1066
export const TAG_TRIGGERVERSION = 1067
export const TAG_TRIGGERFLAGS = 1068
export const TAG_TRIGGERINDEX = 1069
export const TAG_VERIFYSCRIPT = 1079
export const TAG_CHANGELOGTIME = 1080
export const TAG_CHANGELOGNAME = 1081
export const TAG_CHANGELOGTEXT = 1082
export const TAG_BROKENMD5 = 1083
export const TAG_PREREQ = 1084
export const TAG_PREINPROG = 1085
export const TAG_POSTINPROG = 1086
export const TAG_PREUNPROG = 1087
export const TAG_POSTUNPROG = 1088
export const TAG_BUILDARCHS = 1089
export const TAG_OBSOLETENAME = 1090
export const TAG_VERIFYSCRIPTPROG = 1091
export const TAG_TRIGGERSCRIPTPROG = 1092
export const TAG_DOCDIR = 1093
export const TAG_COOKIE = 1094
export const TAG_FILEDEVICES = 1095
export const TAG_FILEINODES = 1096
export const TAG_FILELANGS = 1097
export const TAG_PREFIXES = 1098
export const TAG_INSTPREFIXES = 1099
export const TAG_TRIGGERIN = 1100
export const TAG_TRIGGERUN = 1101
export const TAG_TRIGGERPOSTUN = 1102
export const TAG_AUTOREQ = 1103
export const TAG_AUTOPROV = 1104
export const TAG_CAPABILITY = 1105
export const TAG_SOURCEPACKAGE = 1106
export const TAG_OLDORIGFILENAMES = 1107
export const TAG_BUILDPREREQ = 1108
export const TAG_BUILDREQUIRES = 1109
export const TAG_BUILDCONFLICTS = 1110
export const TAG_BUILDMACROS = 1111
export const TAG_PROVIDEFLAGS = 1112
export const TAG_PROVIDEVERSION = 1113
export const TAG_OBSOLETEFLAGS = 1114
export const TAG_OBSOLETEVERSION = 1115
export const TAG_DIRINDEXES = 1116
export const TAG_BASENAMES = 1117
export const TAG_DIRNAMES = 1118
export const TAG_ORIGDIRINDEXES = 1119
export const TAG_ORIGBASENAMES = 1120
export const TAG_ORIGDIRNAMES = 1121
export const TAG_OPTFLAGS = 1122
export const TAG_DISTURL = 1123
export const TAG_PAYLOADFORMAT = 1124
export const TAG_PAYLOADCOMPRESSOR = 1125
export const TAG_PAYLOADFLAGS = 1126
export const TAG_INSTALLCOLOR = 1127
export const TAG_INSTALLTID = 1128
export const TAG_REMOVETID = 1129
export const TAG_SHA1RHN = 1130
export const TAG_RHNPLATFORM = 1131
export const TAG_PLATFORM = 1132
export const TAG_PATCHESNAME = 1133
export const TAG_PATCHESFLAGS = 1134
export const TAG_PATCHESVERSION = 1135
export const TAG_CACHECTIME = 1136
export const TAG_CACHEPKGPATH = 1137
export const TAG_CACHEPKGSIZE = 1138
export const TAG_CACHEPKGMTIME = 1139
export const TAG_FILECOLORS = 1140
export const TAG_FILECLASS = 1141
export const TAG_CLASSDICT = 1142
export const TAG_FILEDEPENDSX = 1143
export const TAG_FILEDEPENDSN = 1144
export const TAG_DEPENDSDICT = 1145
export const TAG_SOURCEPKGID = 1146
export const TAG_FILECONTEXTS = 1147
export const TAG_FSCONTEXTS = 1148
export const TAG_RECONTEXTS = 1149
export const TAG_POLICIES = 1150
export const TAG_PRETRANS = 1151
export const TAG_POSTTRANS = 1152
export const TAG_PRETRANSPROG = 1153
export const TAG_POSTTRANSPROG = 1154
export const TAG_DISTTAG = 1155
export const TAG_OLDSUGGESTSNAME = 1156
export const TAG_OLDSUGGESTSVERSION = 1157
export const TAG_OLDSUGGESTSFLAGS = 1158
export const TAG_OLDENHANCESNAME = 1159
export const TAG_OLDENHANCESVERSION = 1160
export const TAG_OLDENHANCESFLAGS = 1161
export const TAG_PRIORITY = 1162
export const TAG_CVSID = 1163
export const TAG_BLINKPKGID = 1164
export const TAG_BLINKHDRID = 1165
export const TAG_BLINKNEVRA = 1166
export const TAG_FLINKPKGID = 1167
export const TAG_FLINKHDRID = 1168
export const TAG_FLINKNEVRA = 1169
export const TAG_PACKAGEORIGIN = 1170
export const TAG_TRIGGERPREIN = 1171
export const TAG_BUILDSUGGESTS = 1172
export const TAG_BUILDENHANCES = 1173
export const TAG_SCRIPTSTATES = 1174
export const TAG_SCRIPTMETRICS = 1175
export const TAG_BUILDCPUCLOCK = 1176
export const TAG_FILEDIGESTALGOS = 1177
export const TAG_VARIANTS = 1178
export const TAG_XMAJOR = 1179
export const TAG_XMINOR = 1180
export const TAG_REPOTAG = 1181
export const TAG_KEYWORDS = 1182
export const TAG_BUILDPLATFORMS = 1183
export const TAG_PACKAGECOLOR = 1184
export const TAG_PACKAGEPREFCOLOR = 1185
export const TAG_XATTRSDICT = 1186
export const TAG_FILEXATTRSX = 1187
export const TAG_DEPATTRSDICT = 1188
export const TAG_CONFLICTATTRSX = 1189
export const TAG_OBSOLETEATTRSX = 1190
export const TAG_PROVIDEATTRSX = 1191
export const TAG_REQUIREATTRSX = 1192
export const TAG_BUILDPROVIDES = 1193
export const TAG_BUILDOBSOLETES = 1194
export const TAG_DBINSTANCE = 1195
export const TAG_NVRA = 1196

// https://github.com/rpm-software-management/rpm/blob/-/lib/rpmds.h#L22
export const SENSE_ANY = 0 << 0
export const SENSE_SERIAL = 1 << 0
export const SENSE_LESS = 1 << 1
export const SENSE_GREATER = 1 << 2
export const SENSE_EQUAL = 1 << 3
export const SENSE_PROVIDES = 1 << 4
export const SENSE_POSTTRANS = 1 << 5
export const SENSE_PREREQ = 1 << 6
export const SENSE_PRETRANS = 1 << 7
export const SENSE_INTERP = 1 << 8
export const SENSE_SCRIPT_PRE = 1 << 9
export const SENSE_SCRIPT_POST = 1 << 10
export const SENSE_SCRIPT_PREUN = 1 << 11
export const SENSE_SCRIPT_POSTUN = 1 << 12
export const SENSE_SCRIPT_VERIFY = 1 << 13
export const SENSE_FIND_REQUIRES = 1 << 14
export const SENSE_FIND_PROVIDES = 1 << 15
export const SENSE_TRIGGERIN = 1 << 16
export const SENSE_TRIGGERUN = 1 << 17
export const SENSE_TRIGGERPOSTUN = 1 << 18
export const SENSE_MISSINGOK = 1 << 19
export const SENSE_SCRIPT_PREP = 1 << 20
export const SENSE_SCRIPT_BUILD = 1 << 21
export const SENSE_SCRIPT_INSTALL = 1 << 22
export const SENSE_SCRIPT_CLEAN = 1 << 23
export const SENSE_RPMLIB = 1 << 24
export const SENSE_TRIGGERPREIN = 1 << 25
export const SENSE_KEYRING = 1 << 26
export const SENSE_PATCHES = 1 << 27
export const SENSE_CONFIG = 1 << 28
export const SENSE_META = 1 << 29

// These are my own names that I've changed / invented / derived
export const SENSE_MASK = // was "RPMSENSE_SENSEMASK" ... genius
  SENSE_SERIAL | SENSE_LESS | SENSE_GREATER | SENSE_EQUAL
export const SENSE_TRIGGER = // was "RPMSENSE_TRIGGER"
  SENSE_TRIGGERPREIN | SENSE_TRIGGERIN | SENSE_TRIGGERUN | SENSE_TRIGGERPOSTUN
export const SENSE_SCRIPT = // new, but I think should exist
  SENSE_SCRIPT_BUILD |
  SENSE_SCRIPT_CLEAN |
  SENSE_SCRIPT_INSTALL |
  SENSE_SCRIPT_POST |
  SENSE_SCRIPT_POSTUN |
  SENSE_SCRIPT_PRE |
  SENSE_SCRIPT_PREP |
  SENSE_SCRIPT_PREUN |
  SENSE_SCRIPT_VERIFY
export const SENSE_ALL_REQUIRES = // was "_ALL_REQUIRES_MASK"
  SENSE_FIND_REQUIRES |
  SENSE_INTERP |
  SENSE_KEYRING |
  SENSE_META |
  SENSE_MISSINGOK |
  SENSE_POSTTRANS |
  SENSE_PREREQ |
  SENSE_PRETRANS |
  SENSE_RPMLIB |
  SENSE_SCRIPT

// https://github.com/rpm-software-management/rpm/blob/-/lib/header.c
export const HEADER_MAX_TAGS = (1 << 16) - 1 // was "HEADER_TAGS_MAX"
export const HEADER_MAX_ARRAY = (1 << 20) - 1 // was "HEADER_ARRAY_MAX"
export const HEADER_MAX_DATA = (1 << 28) - 1 // was "HEADER_DATA_MAX"

//https://github.com/rpm-software-management/rpm/blob/-/lib/rpmfiles.h#L45
export const FILE_NONE = 0 << 0
export const FILE_CONFIG = 1 << 0
export const FILE_DOC = 1 << 1
export const FILE_ICON = 1 << 2
export const FILE_MISSINGOK = 1 << 3
export const FILE_NOREPLACE = 1 << 4
export const FILE_SPECFILE = 1 << 5
export const FILE_GHOST = 1 << 6
export const FILE_LICENSE = 1 << 7
export const FILE_README = 1 << 8
export const FILE_UNPATCHED = 1 << 10
export const FILE_PUBKEY = 1 << 11
export const FILE_ARTIFACT = 1 << 12

export const i18ntable = (locales = 'C') => [
  HEADER_I18NTABLE,
  TYPE_STRING_ARRAY,
  locales,
]

const write_rec_idx = (file, tag, type, offset, count = 1) =>
  file.write(
    new Uint8Array([
      (tag >> 24) & 255,
      (tag >> 16) & 255,
      (tag >> 8) & 255,
      tag & 255,
      (type >> 24) & 255,
      (type >> 16) & 255,
      (type >> 8) & 255,
      type & 255,
      (offset >> 24) & 255,
      (offset >> 16) & 255,
      (offset >> 8) & 255,
      offset & 255,
      (count >> 24) & 255,
      (count >> 16) & 255,
      (count >> 8) & 255,
      count & 255,
    ]),
  )

// this is an intended invention of my "fork" of the RPM spec
// let's call it... ummm... "null signal" sure
const write_null_idx = (file, tag, offset, count = 0) =>
  write_rec_idx(
    file,
    tag,
    TYPE_NULL,
    offset,
    Number.isFinite(count) ? count : 'length' in count ? count.length : 0,
  )

// Header format is an array of arrays in the format of
// [tag, type, value] where value can be a single value
// or sometimes an array of values (depending on type).
// The store will be sorted by type, and the index will
// be stored in the order they were given.
// Types are sorted by: 0, 5, 4, 3, 8+9, 6, 1, 2, 7
// This function may not be particularly fast yet, but it'd be nice if it could be.
// Remember to pad before writing the header.
// For int32 and and int64 types the value is expected to be either
// a Number, a BigInt, or an array of either Numbers or BigInts
// int16 and int8 types should have a value that os either a Number or an array of Numbers
// string_array and i18nstring types should have a value that is either a string or an array of strings
// char type is expected to have a String value of length > 0
// binary type is expected to have a Uint8Array value
// NOTES OF MADNESS
// - rpm requires certain types to have a specific byte alignment (for whatever reason)
// - rpm requires all records in the data store to be in the same order they are in the index
//   - but null padding explicitly for type alignment is okay!
// - rpm doesn't allow any extra junk before, after, or in-between declared offsets in the data store
// - rpm requires the magic i18ntable to be before any "translatable strings" (thank goodness it has a low tag id)
// - If... if rpm requires records to be in a specific order and be adjacent to each other AND
//     verify each record's size anyway, why even have an offset field if it's location is already known?!
// - "hdr load: BAD" should be my new busy status
export const write_raw_header_easy_pad = async (file, header) => {
  // detect and add the magic i18ntable table if necessary
  let has_i18n_table = false,
    needs_i18n_table = false
  for (const rec of header) {
    if (rec[1] === TYPE_I18NSTRING) needs_i18n_table = true
    if (rec[0] === HEADER_I18NTABLE) {
      has_i18n_table = true
      break
    }
  }
  if (needs_i18n_table && !has_i18n_table) header.unshift(i18ntable())
  // now begin processing the given header
  const nindex = header.length,
    index = new Buffer(),
    store = new Buffer()
  let offset = 0
  for (let i = 0; i < nindex; ++i)
    // resolve the TYPE_AUTOSTRING
    if (header[i][1] === TYPE_AUTOSTRING) {
      if (header[i].length < 3 || header[i][2] === null)
        (header[i][1] = TYPE_STRING), (header[i][2] = '')
      else if (Array.isArray(header[i][2])) header[i][1] = TYPE_STRING_ARRAY
      else header[i][1] = TYPE_STRING
    } else if (MIN_TYPE > header[i][1] || header[i][1] > MAX_TYPE)
      throw `Error: Unrecognized header type ${header[i][1]}`
    else if (header[i][1] == TYPE_NULL)
      write_null_idx(index, header[0], offset, header[i][2])
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_INT64) {
      const values = arrayify(header[i][2])
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        values.length,
      )
      for (let v of values) {
        v = BigInt(v)
        offset += await store.write(
          new Uint8Array(
            [
              (v >> 56n) & 255n,
              (v >> 48n) & 255n,
              (v >> 40n) & 255n,
              (v >> 32n) & 255n,
              (v >> 24n) & 255n,
              (v >> 16n) & 255n,
              (v >> 8n) & 255n,
              (v >> 0n) & 255n,
            ].map(Number),
          ),
        )
      }
    }
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_INT32) {
      const values = arrayify(header[i][2])
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        values.length,
      )
      for (let v of values) {
        v = BigInt(v)
        offset += await store.write(
          new Uint8Array(
            [
              (v >> 24n) & 255n,
              (v >> 16n) & 255n,
              (v >> 8n) & 255n,
              v & 255n,
            ].map(Number),
          ),
        )
      }
    }
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_INT16) {
      const values = arrayify(header[i][2])
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        values.length,
      )
      for (const v of values)
        offset += await store.write(new Uint8Array([(v >> 8) & 255, v & 255]))
    }
  for (let i = 0; i < nindex; ++i)
    if (
      header[i][1] === TYPE_STRING ||
      header[i][1] === TYPE_STRING_ARRAY ||
      header[i][1] === TYPE_I18NSTRING
    ) {
      const values = arrayify(header[i][2])
      // yes, i know that RPM_TYPE_I18NSTRING is supposed to always have a count of 1
      // even when it's an array with multiple elements, but
      // holy heck RPM... go home you drunk
      // todo: test if validates with official RPM implementation
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        values.length,
      )
      for (const v of values)
        offset += await store.write(new TextEncoder().encode(v + '\0'))
    }
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_CHAR) {
      if (header[i][2].length <= 0) throw `Error: Invalid header value`
      else if (Array.isArray(header[i][2]))
        // value is an array of single chars
        for (const v of header[i][2])
          if (v.length != 1) throw `Error: Invalid header value`
          else
            offset += await store.write(
              new Uint8Array([v.codePointAt(0) & 255]),
            )
      else {
        // value is a single string (recommended)
        const values = new TextEncoder().encode(header[i][2])
        await write_rec_idx(
          index,
          header[i][0],
          header[i][1],
          offset,
          values.length,
        )
        offset += await store.write(values)
      }
    }
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_INT8) {
      const values = arrayify(header[i][2])
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        values.length,
      )
      for (const v of values)
        offset += await store.write(new Uint8Array([v & 255]))
    }
  for (let i = 0; i < nindex; ++i)
    if (header[i][1] === TYPE_BINARY) {
      await write_rec_idx(
        index,
        header[i][0],
        header[i][1],
        offset,
        header[i][2].length,
      )
      offset += await store.write(header[i][2])
    }
  // Output header header, index, and store
  return (
    (await file.write(
      new Uint8Array([
        142,
        173,
        232,
        1,
        0,
        0,
        0,
        0,
        (nindex >> 24) & 255,
        (nindex >> 16) & 255,
        (nindex >> 8) & 255,
        nindex & 255,
        (offset >> 24) & 255,
        (offset >> 16) & 255,
        (offset >> 8) & 255,
        offset & 255,
      ]),
    )) +
    (await file.write(index.bytes())) +
    (await file.write(store.bytes()))
  )
}
