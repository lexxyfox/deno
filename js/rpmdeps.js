import { arrayify } from './util.js'

export const find_deps = async (
  paths,
  args,
  exepath = '/usr/lib/rpm/rpmdeps',
  fs = Deno,
) => {
  if (args === undefined || args.length === 0) return []

  paths = arrayify(paths)
  args = arrayify(args)

  for (let i = args.length; i--; )
    if (args[i] !== undefined) args[i] = '--' + args[i]

  const proc = fs.run({
    cmd: [exepath].concat(args),
    stdin: 'piped',
    stdout: 'piped',
    stderr: 'piped',
  })

  await proc.stdin.write(new TextEncoder().encode(paths.join('\n')))
  await proc.stdin.close()

  const out = new TextDecoder()
    .decode(await Deno.readAll(proc.stdout))
    .split('\n')
    .slice(0, -1)
  const err = new TextDecoder().decode(await Deno.readAll(proc.stderr))

  const { success } = await proc.status()
  await proc.close()

  if (success) return out
  throw err
}

export const find_requires = (paths, args = [], ...rest) =>
  find_deps(paths, ['requires', ...arrayify(args)], ...rest)
