use once_cell::sync::Lazy;

static ARCHIVE_NAME: Lazy<String> =
  Lazy::new(|| format!("deno-{}.zip", env!("TARGET")));

pub fn unpack(
  archive_data: Vec<u8>
) -> Result<std::path::PathBuf, std::io::Error> {
  let temp_dir = secure_tempfile::TempDir::new()?.into_path();
  let archive_path = temp_dir.join("deno.zip");
  let exe_path = temp_dir.join("deno");
  assert!(!exe_path.exists());

  let archive_ext = std::path::Path::new(&*ARCHIVE_NAME)
    .extension().and_then(|ext| ext.to_str()).unwrap();
  if archive_ext != "zip" {
    panic!("Unsupported archive type: '{}'", archive_ext);
  }
  std::fs::write(&archive_path, &archive_data)?;
  let unpack_status = std::process::Command::new("unzip")
    .current_dir(&temp_dir).arg(archive_path).spawn()?.wait()?;
  assert!(unpack_status.success() && exe_path.exists());
  Ok(exe_path)
}
