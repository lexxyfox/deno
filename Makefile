export CI_PROJECT_DIR ?= $(CURDIR)
export CARGO_HOME ?= $(CI_PROJECT_DIR)/rust
export RUSTUP_HOME ?= $(CI_PROJECT_DIR)/rust

export CARGO_TARGET_DIR ?= target
export CARGO_TERM_COLOR ?= always
export CFLAGS := -pipe -Ofast -march=znver3 $(CFLAGS)
export LANG ?= C.UTF-8
export PATH := $(CURDIR)/bin:$(PATH):$(CARGO_HOME)/bin:$(RUSTUP_HOME)/bin:$(CARGO_TARGET_DIR)/release
export POSIXLY_CORRECT ?= yes
export PREFIX ?= /usr
export RUST_BACKTRACE ?= full
export RUSTFLAGS := -C opt-level=3 -C target-cpu=znver3 $(RUSTFLAGS)
export RUSTUP_DIST_SERVER ?= https://static.rust-lang.org
export RUSTUP_INIT_SKIP_PATH_CHECK ?= yes
export RUSTUP_TOOLCHAIN ?= nightly
export RUSTUP_UPDATE_ROOT ?= $(RUSTUP_DIST_SERVER)/rustup

GZIP := pigz -11mnC '$(COM)'
LZMA := xz -9eCnone
PATCH := patch -lfr- -p0
DENO_VER := $(shell sed -rn 's/^version\s*=\s*"(.*)"/\1/p' deno/cli/Cargo.toml)
ZSTD := zstd -22k --ultra --no-check

files := \
	$(CARGO_TARGET_DIR)/release/deno.s \
	$(CARGO_TARGET_DIR)/release/debian-control \
	$(CARGO_TARGET_DIR)/release/debian-control.tar.xz \
	$(CARGO_TARGET_DIR)/release/debian-data.tar \
	$(CARGO_TARGET_DIR)/release/debian-data.tar.xz \
	completions.sh \
	public/binary-amd64/Packages \
	public/binary-amd64/Packages.zst \
	public/cnf/Commands-amd64 \
	public/cnf/Commands-amd64.zst \
	public/Contents-amd64.zst \
	public/deno-$(DENO_VER).deb \
	public/deno-$(DENO_VER).gz \
	public/deno-$(DENO_VER).xz \
	public/deno-$(DENO_VER).zst \
	public/_redirects \
	public/Release \
	sha256sums

.PHONY: all clean mostlyclean install install-strip uninstall rustup-init

.SECONDARY: $(RUSTUP_HOME)/bin/rustup-init

all: \
	public/deno-$(DENO_VER).gz \
	public/deno-$(DENO_VER).rpm \
	public/deno-$(DENO_VER).xz \
	public/deno-$(DENO_VER).zst \
	public/_redirects \
	public/Release

mostlyclean:
	$(RM) $(files)

clean: mostlyclean
	$(RM) '$(CARGO_TARGET_DIR)/release/deno'

rustup-init: $(RUSTUP_HOME)/bin/rustup-init

$(RUSTUP_HOME)/bin/rustup-init:
	mkdir -p '$(@D)'
	curl -Lf '$(RUSTUP_UPDATE_ROOT)/dist/x86_64-unknown-linux-gnu/rustup-init' > '$@'
	strip -s '$@'
	chmod +x '$@'

$(RUSTUP_HOME)/bin/cargo: $(RUSTUP_HOME)/bin/rustup-init
	rustup-init -vy --no-modify-path --default-toolchain nightly --profile minimal

deno/.patched: noupgrade.patch alwaysunstable.patch noreplhello.patch
	$(PATCH) -ddeno < noupgrade.patch
	$(PATCH) -ddeno < alwaysunstable.patch
	$(PATCH) -ddeno < noreplhello.patch
	cp noupgrade.rs deno/cli/tools/upgrade.rs
	touch '$@'

$(CARGO_TARGET_DIR)/release/deno: $(RUSTUP_HOME)/bin/cargo deno/.patched
	cargo build -vv --release --manifest-path deno/Cargo.toml

completions.sh: $(CARGO_TARGET_DIR)/release/deno.s
	$(CARGO_TARGET_DIR)/release/deno.s completions bash | minsh > completions.sh

$(CARGO_TARGET_DIR)/release/deno.s: $(CARGO_TARGET_DIR)/release/deno
	strip -o '$@' -s '$<'

$(CARGO_TARGET_DIR)/release/debian-control: \
	src/debian.conf $(CARGO_TARGET_DIR)/release/deno.s completions.sh src/deno.svg
	cp '$<' '$@'
	echo 'Version: $(DENO_VER)' >> $@
	printf 'Installed-Size: ' >> $@
	deb-size '$(CARGO_TARGET_DIR)/release/deno.s' completions.sh src/deno.desktop src/deno.svg >> $@
	sort -o '$@' '$@'

$(CARGO_TARGET_DIR)/release/debian-control.tar.xz: \
	$(CARGO_TARGET_DIR)/release/debian-control
	tar_copy_from_file '$<' control 0 | $(LZMA) > '$@'

$(CARGO_TARGET_DIR)/release/debian-data.tar: \
	$(CARGO_TARGET_DIR)/release/deno.s completions.sh src/deno.desktop src/deno.svg
	tar_copy_from_file '$<' usr/bin/deno 365 >> '$@'
	tar_copy_from_file completions.sh usr/share/bash-completion/completions/deno 292 >> '$@'
	tar_copy_from_file src/deno.desktop usr/share/applications/deno.desktop 292 >> '$@'
	tar_copy_from_file src/deno.svg usr/share/pixmaps/deno.svg 292 >> '$@'

$(CARGO_TARGET_DIR)/release/debian-data.tar.xz: \
	$(CARGO_TARGET_DIR)/release/debian-data.tar
	$(LZMA) < '$<' > '$@'

public/deno-$(DENO_VER).xz: $(CARGO_TARGET_DIR)/release/deno.s
	$(LZMA) < '$<' > '$@'

public/deno-$(DENO_VER).gz: $(CARGO_TARGET_DIR)/release/deno.s
	$(GZIP) < '$<' > '$@'

public/deno-$(DENO_VER).zst: $(CARGO_TARGET_DIR)/release/deno.s
	$(ZSTD) < '$<' > '$@'

public/binary-amd64/Packages.zst: public/binary-amd64/Packages
	$(ZSTD) < '$<' > '$@'

public/cnf/Commands-amd64.zst: public/cnf/Commands-amd64
	$(ZSTD) < '$<' > '$@'

public/Contents-amd64.zst: public/Contents-amd64
	$(ZSTD) < '$<' > '$@'

public/deno-$(DENO_VER).deb: \
	$(CARGO_TARGET_DIR)/release/debian-control.tar.xz \
	$(CARGO_TARGET_DIR)/release/debian-data.tar.xz
	deb-pack xz '$(CARGO_TARGET_DIR)/release/debian-control.tar.xz' \
	'$(CARGO_TARGET_DIR)/release/debian-data.tar.xz' > '$@'

public/binary-amd64/Packages: public/deno-$(DENO_VER).deb
	mkdir -p '$(@D)'
	cd public && deb-scan ./*.deb > binary-amd64/Packages

public/cnf/Commands-amd64: src/Commands-amd64
	mkdir -p '$(@D)'
	cp '$<' '$@'
	echo 'version: $(DENO_VER)' >> $@

sha256sums: public/Contents-amd64 public/Contents-amd64.zst \
	public/cnf/Commands-amd64 public/cnf/Commands-amd64.zst \
	public/binary-amd64/Packages public/binary-amd64/Packages.zst
	deb-sha256 'public/binary-amd64/Packages' './binary-amd64/Packages' >> '$@'
	deb-sha256 'public/binary-amd64/Packages.zst' './binary-amd64/Packages.zst' >> '$@'
	deb-sha256 'public/cnf/Commands-amd64' './cnf/Commands-amd64' >> '$@'
	deb-sha256 'public/cnf/Commands-amd64.zst' './cnf/Commands-amd64.zst' >> '$@'
	deb-sha256 'public/Contents-amd64' 'Contents-amd64' >> '$@'
	deb-sha256 'public/Contents-amd64.zst' 'Contents-amd64.zst' >> '$@'

public/Release: src/Release sha256sums
	cat < 'src/Release' >> '$@'
	sort -nk2 'sha256sums' >> '$@'
	date -u +'date: %-a, %-d %-b %-Y %-H:%-M:%-S 0' >> '$@'

public/deno-$(DENO_VER).rpm: \
	$(CARGO_TARGET_DIR)/release/deno.s completions.sh src/deno.desktop src/deno.svg
	rpm-kludge deno '$(DENO_VER)' --null-lead --use-rpmdeps --sort-deps \
	--license 'MIT' --summary 'A modern runtime for JavaScript and TypeScript' \
	--file 'usr/bin/deno'                               --load '$(CARGO_TARGET_DIR)/release/deno.s' --mode 365 \
	--file 'usr/share/bash-completion/completions/deno' --load 'completions.sh'        --mode 292 \
	--file 'usr/share/applications/deno.desktop'        --load 'src/deno.desktop'      --mode 292 \
	--file 'usr/share/pixmaps/deno.svg'                 --load 'src/deno.svg'          --mode 292 \
	> '$@'

public/_redirects: src/_redirects
	cp '$<' '$@'
	echo '/deno/deno.deb /deno/deno-$(DENO_VER).deb' >> '$@'
	echo '/deno/deno.gz /deno/deno-$(DENO_VER).gz'   >> '$@'
	echo '/deno/deno.rpm /deno/deno-$(DENO_VER).rpm' >> '$@'
	echo '/deno/deno.xz /deno/deno-$(DENO_VER).xz'   >> '$@'
	echo '/deno/deno.zst /deno/deno-$(DENO_VER).zst' >> '$@'

install: $(CARGO_TARGET_DIR)/release/deno
	install -D '$<' '$(DESTDIR)$(PREFIX)/bin/deno'

install-strip: $(CARGO_TARGET_DIR)/release/deno.s
	install -D '$<' '$(DESTDIR)$(PREFIX)/bin/deno'

uninstall:
	$(RM) '$(DESTDIR)$(PREFIX)/bin/deno'
