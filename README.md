Hewwo!

OwO wats dis??

You've found me on teh internetz :3

This is my foray into CI/CD, and [Deno](https://deno.land)!

I use a CI pipeline to automatically compile Deno, and then package it up in various formats for package management systems to consume. This distro of Deno might be slighly different than the mainsteam however - so far I think I'm valuing some minimal amount of package format compliance first and foremost, then runtime speed, start up time, GNU standards integration, installed size on disk, size over the network, and finally elimination of unnessacary statement puncutation (in computer languages, not natural ones!).

If I'm currently not supporting your favourite GNU distro, then by all means help me out if you can! I'll be happy to merge your pull request if you make one (and attempt to follow the values stated above). I can always make friends with more GNU distros!

You're free to add this repository to your system. CI tests should ensure that the executable continues to function properly. If you're using Debian or Ubuntu or some derivitive thereof, it goes something like this:

```bash
sudo add-apt-repository -y 'deb [arch=amd64 trusted=yes] https://lexxyfox.gitlab.io/deno .. .' && 
sudo apt install deno
```

Otherwise, [you can find more extensive installation instructions over on the wiki (click here).](https://gitlab.com/lexxyfox/deno/wikis/Installation)

Also, did you know? Deno can be used to [create executable *nix scripts with the proper hashbang](https://gitlab.com/lexxyfox/deno/wikis/Hashbang) too!
